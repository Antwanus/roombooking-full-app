package com.av.elizabeth;


import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class Elizabeth {
   private static final LocalDate bDay = LocalDate.of(2021, 7, 8);
   private static final List<PredictableLeap> predictableLeaps = List.of(
       new PredictableLeap(Period.ofWeeks(5), "Leap 1 - sensations"),
       new PredictableLeap(Period.ofWeeks(8), "Leap 2 - patterns"),
       new PredictableLeap(Period.ofWeeks(12), "Leap 3 - transitions"),
       new PredictableLeap(Period.ofWeeks(19), "Leap 4 - events"),
       new PredictableLeap(Period.ofWeeks(26), "Leap 5 - relations"),
       new PredictableLeap(Period.ofWeeks(37), "Leap 6 - categories"),
       new PredictableLeap(Period.ofWeeks(46), "Leap 7 - sequences"),
       new PredictableLeap(Period.ofWeeks(55), "Leap 8 - programmes"),
       new PredictableLeap(Period.ofWeeks(64), "Leap 9 - principles"),
       new PredictableLeap(Period.ofWeeks(75), "Leap 10 - systems")
   );

   public static void main(String[] args) {
      printWeeks(bDay, LocalDate.now(), Period.ofWeeks(1));

   }

   static Function<Period, Optional<PredictableLeap>> findLeapByPeriodFun = period ->
      predictableLeaps.stream()
          .filter((PredictableLeap leap) -> leap.when.equals(period))
          .findFirst();

   static void printWeeks(LocalDate start, LocalDate end, Period period) {
      int counter = 0;
      end = end.plusWeeks(2);
      while (start.isBefore(end)){
         System.out.println("Elizabeth is week " + counter + ", date: " + start);
         findLeapByPeriodFun
             .apply(Period.ofWeeks(counter))
             .ifPresent(System.out::println);
         start = start.plus(period);
         counter++;
      }
   }

   static class PredictableLeap {
      private final Period when;
      private final String description;

      public PredictableLeap(Period when, String description) {
         this.when = when;
         this.description = description;
      }

      @Override
      public String toString() {
         return "PredictableLeap{" +
             "when=" + when +
             ", description='" + description + '\'' +
             '}';
      }
   }
}

package com.av;

public class MySingleton {
   private static MySingleton instance = null;


   private MySingleton() {

   }

   public static MySingleton getInstance() {
      if(instance == null) {
         return new MySingleton();
      } else {
         return instance;
      }
   }
}

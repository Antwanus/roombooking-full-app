package com.av.streams;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class ParallelDemo {
   public static int doubleInt(int n) {
      try {
         Thread.sleep(100);
         System.out.println(Thread.currentThread().getName() + "\twith number " +n);
      } catch (InterruptedException ignored) {      }
      return n*2;
   }

   public static void main(String[] args) {
      List<Integer> intList = Arrays.asList(3, 1, 4, 1, 5, 9);
      // non-functional, shared mutable state
      int total = 0;
      for(int i : intList)
         total += i;
      System.out.println("Total = " + total);

      // Java 8 Streams
      total = 0;
//      IntStream.of(3,1,4,1,5,9)
//          .forEach(n -> total += n);       // does not compile

      total = IntStream.of(3,1,4,1,5,9,2,6)
          .sum();
      System.out.println("Total = " + total);

      Instant before = Instant.now();
      total = IntStream.of(3,1,4,1,5,9,2,6,5,3,5,7,9,7)
//          .parallel()
          .map(ParallelDemo::doubleInt)
          .sum();
      Instant after = Instant.now();
      Duration duration = Duration.between(before, after);
      System.out.println("Total of double ints = " + total);
      System.out.println("time = " + duration.toMillis() + " ms");

   }
}

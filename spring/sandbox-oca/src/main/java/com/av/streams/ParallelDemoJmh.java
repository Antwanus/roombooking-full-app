package com.av.streams;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Thread)
@Fork(value = 2, jvmArgs = {"-Xms4G", "-Xmx4G"})
// Threadpool size: jvmArgs = {"-Djava.util.concurrent.ForkJoinPool.common.parallelism=4"}
public class ParallelDemoJmh {
   public int doubleInt(int i) {
      try {
         Thread.sleep(100);
      } catch (InterruptedException ignored) {}
      return i * 2;
   }

   @Benchmark
   public int doubleAndSumSequential() {
      return IntStream.of(3,1,4,1,5,9)
          .map(this::doubleInt)
          .sum();
   }
   @Benchmark
   public int doubleAndSumParallel() {
      return IntStream.of(3,1,4,1,5,9)
          .parallel()
          .map(this::doubleInt)
          .sum();
   }
}

package com.av.streams;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class CompletableFutureAllOfDemo {
   private int getNextValue(){
      try {
         Thread.sleep((long) (Math.random() * 100));
      } catch (InterruptedException e) {
         throw new RuntimeException(e);
      }
      return 42;
   }

   public CompletableFuture<Integer> getValue(){
      return CompletableFuture.supplyAsync(this::getNextValue);
   }

   public static void main(String[] args) {
      CompletableFutureAllOfDemo demo = new CompletableFutureAllOfDemo();
      CompletableFuture<?>[] completableFutures = Stream.generate(demo::getValue)
          .limit(100)
          .toArray(CompletableFuture[]::new);

      CompletableFuture.allOf(completableFutures).join(); // all the completableFutures (CF) are completed on .join();

      Arrays.stream(completableFutures)
          .map(CompletableFuture::join) // maps CompletableFuture (CF) to whatever the value was coming out of the CF
          .forEach(System.out::println);
   }
}

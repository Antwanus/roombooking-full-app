package com.virtualpairprogrammers.roombooking.rest;

import com.virtualpairprogrammers.roombooking.data.BookingRepository;
import com.virtualpairprogrammers.roombooking.model.entities.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/bookings")
public class RestBookingController {

   @Autowired
   BookingRepository bookingRepo;

//   @GetMapping
//   public List<Booking> getAllBookings() {
//      return bookingRepo.findAll();
//   }
   @GetMapping
   public Booking getBookingById(@RequestParam("id") Long id) {
      return bookingRepo.findById(id).orElse(null);
   }

   @DeleteMapping("/{id}")
   public void deleteBooking(@PathVariable("id") Long id) throws InterruptedException {
      Thread.sleep(2000);
      Optional<Booking> found = bookingRepo.findById(id);
      found.ifPresent(booking -> bookingRepo.delete(booking));
   }

   @GetMapping("/{date}")
   public List<Booking> getBookingsByDate(@PathVariable("date") String date) throws InterruptedException {
      Thread.sleep(2000);
      return bookingRepo.findAllByDate(Date.valueOf(date));
   }
}
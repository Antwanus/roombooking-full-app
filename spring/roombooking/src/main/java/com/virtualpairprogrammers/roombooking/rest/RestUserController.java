package com.virtualpairprogrammers.roombooking.rest;

import com.virtualpairprogrammers.roombooking.data.UserRepository;
import com.virtualpairprogrammers.roombooking.model.dto.AngularUser;
import com.virtualpairprogrammers.roombooking.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/users")
public class RestUserController {
   @Autowired  private UserRepository userRepo;

   @GetMapping
   public List<AngularUser> getAllUsers() {
      return userRepo.findAll()
         .stream()
          .map(AngularUser::new)
          .collect(Collectors.toList());
   }

   @PostMapping
   public AngularUser postNewUser(@RequestBody User u) {
      if (u != null) {
         return new AngularUser(userRepo.save(u));
      } else return null;
   }
   @PutMapping
   public AngularUser putUpdateUser(@RequestBody AngularUser u) throws InterruptedException {
      Thread.sleep(2000);
//      throw new RuntimeException("Something went wrong");
      Optional<User> found = userRepo.findById(u.getId());
      if (found.isPresent()) {
         found.get().setName(u.getName());
         return new AngularUser(userRepo.save(found.get()));
      } else return null;
   }
   @GetMapping("{id}")
   public AngularUser getUserById(@PathVariable("id") Long id) {
      Optional<User> found = userRepo.findById(id);
      return found.map(AngularUser::new)
                  .orElse(null);
   }
   @DeleteMapping("{id}")
   public void deleteUserById(@PathVariable("id") Long id) {
      Optional<User> found = userRepo.findById(id);
      found.ifPresent(u -> userRepo.delete(u));
   }

   @GetMapping("/resetpw/{id}")
   public void resetPassword(@PathVariable("id") Long id) {
      User user = userRepo.findById(id).get();
      user.setPassword("secret");
      userRepo.save(user);

   }


}

package com.virtualpairprogrammers.roombooking.model.dto;

import com.virtualpairprogrammers.roombooking.model.entities.User;

public class AngularUser {
   private long id;
   private String name;

   public AngularUser() { }
   public AngularUser(User u) {
      this.id = u.getId();
      this.name = u.getName();
   }

   public User toUserEntity() {
      return new User(id, name, "");
   }

   public long getId() { return id; }
   public void setId(long id) { this.id = id; }
   public String getName() { return name; }
   public void setName(String name) { this.name = name; }
}

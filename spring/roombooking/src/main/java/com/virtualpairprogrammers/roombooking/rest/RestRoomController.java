package com.virtualpairprogrammers.roombooking.rest;

import com.virtualpairprogrammers.roombooking.data.RoomRepository;
import com.virtualpairprogrammers.roombooking.model.entities.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/rooms")
public class RestRoomController {

   @Autowired
   RoomRepository roomRepo;

//testing
//   @GetMapping
//   public List<Room> getAllRooms(HttpServletResponse res) throws InterruptedException {
//      res.setStatus(402);
//      return null;
//   }
   @GetMapping
   public List<Room> getAllRooms() throws InterruptedException {
      Thread.sleep(2000L);
      return roomRepo.findAll();
   }
   @PostMapping
   public Room newRoom(@RequestBody Room r) throws InterruptedException {
      Thread.sleep(2000L);
      return roomRepo.save(r);
   }
   @PutMapping
   public Room updateRoom(@RequestBody Room r) {
      Optional<Room> found = roomRepo.findById(r.getId());
      if(found.isPresent()) {
         found.get().setCapacities(r.getCapacities());
         found.get().setLocation(r.getLocation());
         found.get().setName(r.getName());
         return roomRepo.save(found.get());
      } else return null;
   }

   @GetMapping("/{id}")
   public Room getRoomById(@PathVariable("id") Long id){
      Optional<Room> byId = roomRepo.findById(id);
      return byId.orElse(null);
   }
   @DeleteMapping("/{id}")
   public void deleteRoom(@PathVariable("id") Long id) {
      System.out.println("WTF ?  => " +id);
      Optional<Room> found = roomRepo.findById(id);
      found.ifPresent(room -> roomRepo.delete(room));
   }

}

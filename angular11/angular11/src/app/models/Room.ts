export class Room {
  id: number;
  name: string;
  location: string;
  capacities: Array<LayoutCapacity>; //TODO in construction....

  constructor(id?: number, name?: string, location?: string) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.capacities = new Array<LayoutCapacity>();
  }

  getId(): number {
    return this.id;
  }
  static fromHttp(r: Room): Room {
    const newR = new Room();
    newR.id = r.id;
    newR.name = r.name;
    newR.location = r.location;
    // newR.capacities = r.capacities; //=> does not work
    newR.capacities = new Array<LayoutCapacity>();
    for (const e of r.capacities) {
      newR.capacities.push(LayoutCapacity.fromHttp(e));
    }
    return newR;
  }
}
export class LayoutCapacity {
  id: number;
  layout: Layout;
  capacity: number;

  constructor(id?: number, capacity?: number, layout?: Layout) {
    this.id = id;
    this.capacity = capacity;
    this.layout = layout;
  }
  static fromHttp(lc: LayoutCapacity): LayoutCapacity {
    const newLC = new LayoutCapacity();
    newLC.id = lc.id;
    newLC.capacity = lc.capacity;
    newLC.layout = Layout[lc.layout];
    return newLC;
  }
}
export enum Layout {
  THEATER = 'Theater',
  USHAPE = 'U-Shape',
  BOARD = 'Board Meeting',
}

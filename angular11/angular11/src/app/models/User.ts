export class User {
  id: number;
  name: string;

  constructor(id?: number, name?: string) {
    this.id = id;
    this.name = name;
  }

  static fromHttp(u: User): User {
    const newU = new User();
    newU.id = u.id;
    newU.name = u.name;
    return newU;
  }

  getRole(): String {
    return 'bankai';
  }
}

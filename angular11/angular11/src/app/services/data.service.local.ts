import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RoomsComponent } from '../components/admin/rooms/rooms.component';
import { Booking } from '../models/Booking';
4;
import { Layout, LayoutCapacity, Room } from '../models/Room';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  bookings: Array<Booking>;
  rooms: Array<Room>;
  users: Array<User>;

  getAllBookings(): Observable<Array<Booking>> {
    return of(this.bookings);
  }
  getBookings(date: string): Observable<Array<Booking>> {
    return of(this.bookings.filter((b) => b.date === date));
  }
  getBookingsOfDate(d: string): Observable<Array<Booking>> {
    return of(this.bookings.filter((e) => e.date == d));
  }
  getBookingById(id: number): Observable<Booking> {
    return of(this.bookings.find((b) => b.id == id));
  }
  updateBooking(b: Booking): Observable<Booking> {
    var found = this.bookings.find((e) => e.id === b.id);
    if (found) {
      found.id = b.id;
      found.date = b.date;
      found.title = b.title;
      found.user = b.user;
      found.startTime = b.startTime;
      found.endTime = b.endTime;
      found.room = b.room;
      found.layout = b.layout;
      found.participants = b.participants;
      return of(found);
    }
  }
  saveNewBooking(b: Booking): Observable<Booking> {
    let id = 0;
    this.bookings.forEach((b) => {
      if (b.id > id) {
        id = b.id;
      }
    });
    b.id = id + 1;
    this.bookings.push(b);
    return of(b);
  }
  deleteBookingById(id: number): Observable<any> {
    var found = this.bookings.find((b) => b.id == id);
    console.log(this.bookings);

    this.bookings.splice(this.bookings.indexOf(found), 1);
    console.log(this.bookings);

    return of(null);
  }

  getRooms(): Observable<Array<Room>> {
    return of(this.rooms);
  }
  getRoomById(id: number): Room {
    return this.rooms.find((r) => r.id === id);
  }
  deleteRoom(id: number): Observable<boolean> {
    const found = this.getRoomById(id);
    const indexOfFound = this.rooms.indexOf(found);
    if (found) {
      this.rooms.splice(indexOfFound, 1);
      return of(true);
    }
    return of(false);
  }
  updateRoom(r: Room): Observable<Room> {
    let found = this.rooms.find((e) => e.id === r.id);
    if (found) {
      found.name = r.name;
      found.location = r.location;
      found.capacities = r.capacities;
    }
    return of(found);
  }
  saveNewRoom(r: Room): Observable<Room> {
    let newId = 0;
    for (const e of this.rooms) {
      if (e.id > newId) {
        newId = e.id;
      }
    }
    const newRoom = new Room();
    newRoom.id = ++newId;
    newRoom.capacities = r.capacities;
    newRoom.name = r.name;
    newRoom.location = r.location;
    this.rooms.push(newRoom);
    return of(newRoom);
  }
  getUsers(): Observable<Array<User>> {
    return of(this.users);
  }
  deleteUser(id: number): Observable<any> {
    const user = this.users.find((e) => e.id === id);
    this.users.splice(this.users.indexOf(user), 1);
    return of(null);
  }
  resetUserPassword(id: number): Observable<any> {
    console.log('TODO: resetUserPassword() has not been implemented yet');
    return of(null);
  }
  updateUser(u: User): Observable<User> {
    const found = this.users.find((e) => e.id === u.id);
    found.name = u.name;
    return of(found);
  }
  saveNewUser(u: User, pw: string): Observable<User> {
    let id = 0;
    for (const e of this.users) {
      if (e.id > id) {
        id = e.id;
      }
    }
    u.id = ++id;
    this.users.push(u);
    return of(u);
  }

  /** bootstrapping dummy data */
  constructor() {
    console.log(environment);

    const room1 = new Room(1, 'First Room', 'First Location');
    room1.capacities.push(new LayoutCapacity(1, 50, Layout.BOARD));
    room1.capacities.push(new LayoutCapacity(2, 70, Layout.THEATER));
    room1.capacities.push(new LayoutCapacity(3, 40, Layout.USHAPE));
    const room2 = new Room(2, 'Second Room', 'Second Location');
    room2.capacities.push(new LayoutCapacity(4, 30, Layout.BOARD));
    room2.capacities.push(new LayoutCapacity(5, 50, Layout.THEATER));
    this.rooms = new Array<Room>();
    this.rooms.push(room1);
    this.rooms.push(room2);
    /*------------------------------------------------------------------*/
    this.users = new Array<User>();
    this.users.push(new User(1, 'user1'));
    this.users.push(new User(2, 'user2'));
    this.users.push(new User(3, 'user3'));
    this.users.push(new User(4, 'user4'));

    this.bookings = new Array<Booking>();
    this.bookings.push(
      new Booking(
        1,
        room1,
        this.users[0],
        Layout.BOARD,
        'testBooking1',
        formatDate(new Date(), 'yyyy-MM-dd', 'en-GB'),
        '13:00',
        '14:00',
        100
      )
    );
    this.bookings.push(
      new Booking(
        2,
        room2,
        this.users[1],
        Layout.USHAPE,
        'testBooking2',
        formatDate(new Date(), 'yyyy-MM-dd', 'en-GB'),
        '14:00',
        '15:00',
        200
      )
    );
  }
}

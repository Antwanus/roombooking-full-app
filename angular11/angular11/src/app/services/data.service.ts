import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Booking } from '../models/Booking';
import { Layout, Room } from '../models/Room';
import { User } from '../models/User';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {
    console.log('new DataService! environment = ', environment);
    console.log('httpClient = ', http);
    console.log('environment.restUrl = ', environment.restUrl);
  }

  // BOOKING
  getAllBookings(): Observable<Array<Booking>> {
    return this.http
      .get<Array<Booking>>(environment.restUrl + '/api/bookings/')
      .pipe(
        map((httpBookings) => {
          const bookings = new Array<Booking>();
          for (const httpBooking of httpBookings) {
            bookings.push(Booking.fromHttp(httpBooking));
          }
          return bookings;
        })
      );
  }
  getBookingsOfDate(date: string): Observable<Array<Booking>> {
    return this.http.get<Array<Booking>>(
      environment.restUrl + '/api/bookings/' + date
    );
  }
  getBookingById(id: number): Observable<Booking> {
    return this.http
      .get<Booking>(environment.restUrl + '/api/bookings?id=' + id)
      .pipe(map((b) => Booking.fromHttp(b)));
  }
  updateBooking(b: Booking): Observable<Booking> {
    console.log('TODO: resetUserPassword() has not been implemented yet');
    return of(null);
  }
  saveNewBooking(b: Booking): Observable<Booking> {
    console.log('TODO: resetUserPassword() has not been implemented yet');
    return of(null);
  }
  deleteBookingById(id: number): Observable<any> {
    return this.http.delete(environment.restUrl + '/api/bookings/' + id);
  }

  // ROOM
  getRooms(): Observable<Array<Room>> {
    return this.http.get<Array<Room>>(environment.restUrl + '/api/rooms/').pipe(
      map((httpRooms) => {
        const rooms = new Array<Room>();
        for (const httpRoom of httpRooms) {
          rooms.push(Room.fromHttp(httpRoom));
        }
        return rooms;
      })
    );
  }
  getRoomById(id: number): Observable<Room> {
    return this.http.get<Room>(environment.restUrl + '/api/rooms/' + id);
  }
  deleteRoom(id: number): Observable<any> {
    console.log('TODO: resetUserPassword() has not been implemented yet');
    return this.http.delete(environment.restUrl + '/api/rooms/' + id);
  }
  updateRoom(r: Room): Observable<Room> {
    return this.http.put<Room>(
      environment.restUrl + '/api/rooms/',
      this.getRoomDTO(r)
    );
  }
  saveNewRoom(r: Room): Observable<Room> {
    return this.http.post<Room>(
      environment.restUrl + '/api/rooms/',
      this.getRoomDTO(r)
    );
  }
  private getRoomDTO(r: Room) {
    const roomDTO = {
      id: r.id,
      name: r.name,
      location: r.location,
      capacities: [],
    };
    for (const lc of r.capacities) {
      let correctLayout;
      for (const key in Layout) {
        if (Layout[key] === lc.layout) {
          correctLayout = key;
        }
      }
      let correctedLayout = { layout: correctLayout, capacity: lc.capacity };
      roomDTO.capacities.push(correctedLayout);
    }
    return roomDTO;
  }

  // USER
  getUsers(): Observable<Array<User>> {
    return this.http.get<Array<User>>(environment.restUrl + '/api/users/').pipe(
      map((data) => {
        const users = new Array<User>();
        data.forEach((e) => users.push(User.fromHttp(e)));
        return users;
      })
    );
  }
  deleteUser(id: number): Observable<any> {
    return this.http.delete(environment.restUrl + '/api/users/' + id);
  }
  resetUserPassword(id: number): Observable<any> {
    return this.http.get(environment.restUrl + '/api/users/resetpw/' + id);
  }
  updateUser(u: User): Observable<User> {
    return this.http.put<User>(environment.restUrl + '/api/users/', u);
  }
  saveNewUser(u: User, pw: string): Observable<User> {
    const userDTO = { id: u.id, name: u.name, password: pw };
    return this.http.post<User>(environment.restUrl + '/api/users/', userDTO);
  }
}

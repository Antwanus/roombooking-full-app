import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomsComponent } from './components/admin/rooms/rooms.component';
import { UsersComponent } from './components/admin/users/users.component';
import { BookingEditComponent } from './components/bookings/booking-edit/booking-edit.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { EditBookingLoadComponent } from './components/calendar/edit-booking-load/edit-booking-load.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'admin/users', component: UsersComponent },
  { path: 'admin/rooms', component: RoomsComponent },
  { path: '', component: CalendarComponent },
  { path: 'editBooking', component: BookingEditComponent },
  { path: 'loadEditBooking', component: EditBookingLoadComponent },
  { path: 'addBooking', component: BookingEditComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}

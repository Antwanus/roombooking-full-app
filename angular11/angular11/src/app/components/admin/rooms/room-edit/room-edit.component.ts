import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { nextTick } from 'process';
import { Subscription } from 'rxjs';
import { Layout, LayoutCapacity, Room } from 'src/app/models/Room';
import { DataService } from 'src/app/services/data.service';
import { FormResetService } from 'src/app/services/form-reset.service';

@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.css'],
})
export class RoomEditComponent implements OnInit, OnDestroy {
  @Input() room: Room;

  @Output() dataChangedEvent = new EventEmitter();
  msg = '';

  layouts = Object.keys(Layout);
  layoutEnum = Layout;

  formGroup: FormGroup;
  resetEventSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private formResetService: FormResetService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    this.resetEventSubscription =
      this.formResetService.resetRoomFormEvent.subscribe((nextRoom) => {
        this.room = nextRoom;
        this.initializeForm();
      });
  }
  ngOnDestroy(): void {
    this.resetEventSubscription.unsubscribe();
  }

  private initializeForm() {
    this.formGroup = this.formBuilder.group({
      nameFormControl: [
        this.room.name,
        [Validators.required, Validators.minLength(1)],
      ],
      locationFormControl: [
        this.room.location,
        [Validators.required, Validators.minLength(1)],
      ],
    });
    for (const layout of this.layouts) {
      const layoutCapacity = this.room.capacities.find(
        (lc) => lc.layout === Layout[layout]
      );
      const initialCapacity =
        layoutCapacity == null ? 0 : layoutCapacity.capacity;
      this.formGroup.addControl(
        `layout${layout}`,
        this.formBuilder.control(initialCapacity)
      );
    }
  }

  onSubmit() {
    this.msg = 'Saving...';
    this.room.name = this.formGroup.controls['nameFormControl'].value;
    this.room.location = this.formGroup.value['locationFormControl'];

    this.room.capacities = new Array<LayoutCapacity>();
    for (const layout of this.layouts) {
      const lc = new LayoutCapacity();
      lc.layout = Layout[layout];
      lc.capacity = this.formGroup.value[`layout${layout}`];
      this.room.capacities.push(lc);
    }

    if (this.room.id == null) {
      // NEW ROOM
      this.dataService.saveNewRoom(this.room).subscribe(
        (next) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'rooms'], {
            queryParams: { id: next.id, action: 'view' },
          });
          // this.room.id = next.id;
        },
        (error) => {
          console.log(error.message);
          this.msg = 'Something went wrong :(';
        }
      );
    } else {
      this.dataService.updateRoom(this.room).subscribe(
        (next) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'rooms'], {
            queryParams: { id: next.id, action: 'view' },
          });
        },
        (error) => {
          console.log(error.message);
          this.msg = 'Something went wrong :(';
        }
      );
    }
  }
}

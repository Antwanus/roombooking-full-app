import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Room } from 'src/app/models/Room';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css'],
})
export class RoomDetailComponent {
  @Input() room: Room;
  @Output() dataChangedEvent = new EventEmitter();
  msg = '';

  constructor(private router: Router, private dataService: DataService) {}

  editRoom() {
    this.router.navigate(['admin', 'rooms'], {
      queryParams: { action: 'edit', id: this.room.id },
    });
  }
  deleteRoom() {
    const isConfirmed:boolean = confirm('Delete this room?');
    if(isConfirmed) {
      this.msg = 'Deleting room...';
      this.dataService.deleteRoom(this.room.id).subscribe(
        (next) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'rooms']);
        },
        (error) => {
          this.msg = 'Something went wrong :( Please try again later';
        }
      );

    }
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Room } from 'src/app/models/Room';
import { DataService } from 'src/app/services/data.service';
import { FormResetService } from 'src/app/services/form-reset.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css'],
})
export class RoomsComponent implements OnInit {
  rooms: Array<Room>;
  selectedRoom: Room;
  action: string;

  isDataLoaded = false;
  msg: string = 'Loading data...';
  reloadAttempts = 0;

  constructor(
    private dataService: DataService,
    private formResetService: FormResetService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  private handleUrlParams() {
    this.activeRoute.queryParams.subscribe((params) => {
      console.log('this.activeRoute.queryParams has published data!', params);
      this.action = null;
      const idParam = params['id'];
      if (idParam) {
        //EDIT
        this.selectedRoom = this.rooms.find((e) => e.id === +idParam); // by pre-appending + we're casting it to a number <3
        this.action = params['action'];
      }
      if (params['action'] === 'add') {
        this.selectedRoom = new Room();
        this.formResetService.resetRoomFormEvent.emit(this.selectedRoom);
        this.action = 'edit';
      }
    });
  }
  private loadData() {
    this.dataService.getRooms().subscribe(
      (rooms) => {
        this.rooms = rooms;
        this.isDataLoaded = true;
        this.handleUrlParams();
      },
      (error) => {
        console.log(error);
        if (error.status === 402) {
          this.msg = 'PAY UP';
        } else {
          this.reloadAttempts++;
          if (this.reloadAttempts < 10) {
            this.msg = 'Could not retrieve data.. Trying again.. (' + this.reloadAttempts +')';
            this.loadData();
          } else {
            this.msg = 'Something went wrong...';
          }
        }
      }
    );
  }

  /** By clicking on the button, we are only changing the address to have a query 'admin/rooms?id={id}'
   *  => this be picked up by our subscription on ActivatedRoute.queryParams
   */
  setSelectedRoom(id: number) {
    this.router.navigate(['admin', 'rooms'], {
      queryParams: { id: id, action: 'view' },
    });
  }

  addRoom() {
    this.router.navigate(['admin', 'rooms'], {
      queryParams: { action: 'add' },
    });
  }
}

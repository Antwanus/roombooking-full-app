import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent {
  @Input() u: User;
  @Output() dataChangedEvent = new EventEmitter();
  msg = '';

  constructor(private dataService: DataService, private router: Router) {}

  editUser(): void {
    this.router.navigate(['admin', 'users'], {
      queryParams: { action: 'edit', id: this.u.id },
    });
  }
  deleteUser() {
    const isConfirmed:boolean = confirm('Delete this user?');
    if(isConfirmed) {
      this.msg = 'Deleting user...';
      this.dataService.deleteUser(this.u.id).subscribe(
        (next) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'users']);
        },
        (error) => {
          this.msg = 'Something went wrong :(';
        }
      );
    }
  }
  resetPassword() {
    this.msg = 'Resetting password...';
    this.dataService.resetUserPassword(this.u.id).subscribe(
      (next) => {
        this.msg = 'Success!';
      },
      (error) => {
        this.msg = 'Something went wrong :(';
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';
import { FormResetService } from 'src/app/services/form-reset.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: User[];
  selectedUser: User;
  action: string;

  isDataLoaded = false;
  msg = 'Loading data...';

  constructor(
    private dataService: DataService,
    private formResetService: FormResetService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.action = '';
  }

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {
    this.dataService.getUsers().subscribe(
      (next) => {
        this.users = next;
        this.isDataLoaded = true;
        this.handleUrlParams();
      },
      (error) => {
        this.msg = 'Something went wrong...';
        console.log(error);
      }
    );
  }
  private handleUrlParams() {
    this.activatedRoute.queryParams.subscribe((params) => {
      const idParam = params['id'];
      this.action = params['action'];
      if (idParam) {
        this.selectedUser = this.users.find((e) => e.id === +idParam);
      }
    });
  }

  addNewUser() {
    this.selectedUser = new User();
    this.router.navigate(['admin', 'users'], {
      queryParams: { action: 'add' },
    });
    this.formResetService.resetUserFormEvent.emit(this.selectedUser);
  }
  setSelectedUser(id: number) {
    this.router.navigate(['admin', 'users'], {
      queryParams: { id: id, action: 'view' },
    });
  }
}

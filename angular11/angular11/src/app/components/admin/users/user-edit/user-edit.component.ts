import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';
import { FormResetService } from 'src/app/services/form-reset.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
})
export class UserEditComponent implements OnInit, OnDestroy {
  @Input() u: User;
  formUser: User;

  @Output()
  dataChangedEvent = new EventEmitter();

  warningMsg: string;

  password: string;
  confirmationPassword: string;

  passwordsMatch: boolean = false;
  passwordsAreValid: boolean = false;
  nameIsValid: boolean = false;

  userFormResetSubscription: Subscription;

  constructor(
    private dataService: DataService,
    private router: Router,
    private formResetService: FormResetService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    this.userFormResetSubscription =
      this.formResetService.resetUserFormEvent.subscribe((nextUser) => {
        this.u = nextUser;
        this.initializeForm();
      });
  }
  private initializeForm(): void {
    this.formUser = Object.assign({}, this.u); //=== copyOf(target, source)
    this.checkIfNameIsValid();
    this.checkIfPasswordsAreValidAndMatch();
  }

  ngOnDestroy(): void {
    this.userFormResetSubscription.unsubscribe();
  }

  checkIfPasswordsAreValidAndMatch(): void {
    if (this.formUser.id != null) {
      //=> EDIT SELECTED_USER ==> NO VALIDATION REQUIRED
      this.passwordsAreValid = true;
      this.passwordsMatch = true;
    } else {
      //=> CREATING NEW USER ==> DO_VALIDATE
      this.passwordsMatch = this.password === this.confirmationPassword;
      if (this.password) {
        this.passwordsAreValid = this.password.trim().length > 0;
      }
    }
  }

  checkIfNameIsValid(): void {
    this.nameIsValid = this.formUser.name?.trim().length > 0;
  }
  onFormSubmit(): void {
    this.warningMsg = 'saving data...';
    if (this.formUser.id == null) {
      // => CREATE NEW USER
      this.dataService.saveNewUser(this.formUser, this.password).subscribe(
        (nextUser) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'users'], {
            queryParams: { action: 'view', id: nextUser.id },
          });
        },
        (error) => {
          console.log(error.message);
          this.warningMsg = 'Something went wrong... :(';
        }
      );
    } else {
      // => UPDATE SELECTED_USER
      this.dataService.updateUser(this.formUser).subscribe(
        (next) => {
          this.dataChangedEvent.emit();
          this.router.navigate(['admin', 'users'], {
            queryParams: { action: 'view', id: next.id },
          });
        },
        (error) => {
          console.log(error.message);
          this.warningMsg = 'Something went wrong... :(';
        }
      );
    }
  }
}

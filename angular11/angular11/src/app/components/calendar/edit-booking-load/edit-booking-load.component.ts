import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EditBookingDataService } from 'src/app/services/edit-booking-data.service';

@Component({
  selector: 'app-edit-booking-load',
  templateUrl: './edit-booking-load.component.html',
  styleUrls: ['./edit-booking-load.component.css'],
})
export class EditBookingLoadComponent implements OnInit {
  constructor(
    private editBookingDataService: EditBookingDataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    setTimeout(() => this.navigateWhenReady(), 1000);
  }
  navigateWhenReady() {
    // check data is loaded
    if (this.editBookingDataService.dataLoaded === 2) {
      // +  => navigate
      const id = this.activatedRoute.snapshot.queryParams['id'];
      if(id) {  // EDITING A BOOKING
        this.router.navigate(['loadEditBooking'], { queryParams: { id } });
      } else {  // ADDING A BOOKING
        this.router.navigate(['addBooking']);
      }
    } else {
      // -  => wait & try again
      setTimeout(() => this.navigateWhenReady(), 1000);
    }
  }
}

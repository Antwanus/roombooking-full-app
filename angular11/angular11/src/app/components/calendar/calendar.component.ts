import { formatDate, LowerCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Booking } from 'src/app/models/Booking';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent implements OnInit {
  selectedDate: string;
  bookings: Array<Booking>;
  isDataLoaded: boolean = false;
  msg: string = '';

  constructor(
    private dataService: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.msg = 'Loading data...';
    this.activatedRoute.queryParams.subscribe((params) => {
      this.selectedDate = params['date'];
      if (!this.selectedDate) {
        this.selectedDate = formatDate(new Date(), 'yyyy-MM-dd', 'en-GB');
      }
      this.dataService.getBookingsOfDate(this.selectedDate).subscribe(
        (next) => {
          console.log('dataService.getBookingsOfDate() returns =>', next);
          this.msg = '';
          this.bookings = next;
          this.isDataLoaded = true;
        },
        (error) => {
          this.msg = 'Something has gone wrong :(';
        }
      );
    });
  }

  formatDate(date: string): Date {
    return new Date(date);
  }

  dateChanged() {
    console.log(this.selectedDate);
    this.router.navigate([''], {
      queryParams: { date: this.selectedDate },
    });
  }
  addBooking() {
    this.router.navigate(['loadEditBooking']);
  }
  editBooking(id: number) {
    this.router.navigate(['loadEditBooking'], { queryParams: { id: id } });
  }
  deleteBooking(id: number) {
    const isConfirmed:boolean = confirm('Cancel this booking?');
    if (isConfirmed) {
      this.msg = 'sending delete request';

      this.dataService.deleteBookingById(id).subscribe(
        (next) => {
          this.msg = ''
          this.loadData();
        },
        (error) => {}
      );
    }
  }
}

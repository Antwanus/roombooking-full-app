import { Component, Input, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { Booking } from 'src/app/models/Booking';
import { Layout, Room } from 'src/app/models/Room';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';
import { EditBookingDataService } from 'src/app/services/edit-booking-data.service';

@Component({
  selector: 'app-booking-edit',
  templateUrl: './booking-edit.component.html',
  styleUrls: ['./booking-edit.component.css'],
})
export class BookingEditComponent implements OnInit {
  booking: Booking;
  rooms: Array<Room>;
  layouts: Array<String>;
  layoutEnum = Layout;
  users: Array<User>;

  isDataLoaded: boolean = false;
  msg: string = 'Please wait..';

  constructor(
    private dataService: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private editBookingDataService: EditBookingDataService
  ) {}

  ngOnInit(): void {
    this.layouts = Object.keys(Layout);

    this.rooms = this.editBookingDataService.rooms;
    this.users = this.editBookingDataService.users;
    /**
    this.dataService.getRooms().subscribe((next) => {
      this.rooms = next;
    });

    this.dataService.getUsers().subscribe((next) => {
      this.users = next;
    });
    */

    this.activatedRoute.queryParams.subscribe((queryParams) => {
      const idParam = queryParams['id'];
      if (idParam) {
        this.dataService.getBookingById(idParam).subscribe((nextBooking) => {
          this.isDataLoaded = true;
          this.booking = nextBooking;
          this.msg = '';
          console.log('dataService.getBookingById(id) => ', nextBooking);
        });
      } else {
        this.booking = new Booking();
        this.isDataLoaded = true;
        this.msg = '';
      }
    });
  }

  onSubmit() {
    if (this.activatedRoute.snapshot.queryParams['id']) {
      this.dataService.updateBooking(this.booking).subscribe((next) => {
        this.router.navigate(['']);
      });
    } else {
      this.dataService.saveNewBooking(this.booking).subscribe((next) => {
        this.router.navigate(['']);
      });
    }
  }
}
